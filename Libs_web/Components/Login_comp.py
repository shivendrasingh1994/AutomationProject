import time

from playwright.sync_api import sync_playwright

class Login:
    def __init__(self):
        pass

    def navigate_to_web_page(self,page,url):
        page.goto(url)
        time.sleep(1)

    def write_words_to_search_box(self,page,words):
        input_field = page.locator("//textarea[@aria-label='Search']")
        input_field.fill(words)
        page.keyboard.press("Enter")
        time.sleep(2)




