import pytest
from pytest_bdd import scenario, given, when, then, scenarios
from pytest_bdd import parsers
from playwright.sync_api import Page

from Libs.Components.Login_comp import Login


@scenario('../features/navigate_google.feature',"user verify google home page")
def test_user_verify_google_home_page_header():
    pass

################################################################################################

@given(parsers.parse('user initilize the browser'),target_fixture="url")
def user_get_the_base_url(context):
    url="https://www.google.co.in/"
    return url



@when('user hit google url')
def user_hits_web_page(page : Page,url,login):
    login.navigate_to_web_page(page,url)

@when(parsers.parse('user enter "{words}"'))
def user_hits_web_page(page : Page,words,login):
    login.write_words_to_search_box(page,words)

@then(parsers.parse('user verify "{word}" header'))
def user_verify_entered_text(page : Page,word,login):
    assert word in page.get_by_text("Playwright: Fast and reliable end-to-end testing for modern ...").all_inner_texts()[0]







